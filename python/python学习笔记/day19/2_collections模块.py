# 常用模块一：
# collections模块
# 时间模块
# random模块：随机数模块
# os模块：和操作系统打交道的模块
# sys模块：和python解释器打交道的模块
# 序列化模块：python中的数据类型和str转换的模块
# re模块

# 常用模块二：
# hashlib模块
# configparse模块
# logging模块


# 该章节主讲collections模块：
# 在内置数据类型（dict、list、set、tuple）的基础上，collections模块还提供了几个额外的数据类型：Counter、deque、defaultdict、namedtuple和OrderedDict等。
# 1.namedtuple: 生成可以使用名字来访问元素内容的tuple
# 2.deque: 双端队列，可以快速的从另外一侧追加和推出对象
# 3.Counter: 计数器，主要用来计数
# 4.OrderedDict: 有序字典
# 5.defaultdict: 带有默认值的字典

# 基本数据类型：
# 列表、元组
# 字典
# 集合、frozenset
# 字符串
# 堆栈 先进后出
# 队列：先进先出

# 案例1：namedtuple
# from collections import namedtuple
# Point = namedtuple('point',['x','y'])   ##'point'代表命名元组的名称
# p = Point(1,2)
# print(p.x)   ##结果为：1
# print(p.y)   ##结果为：2
# print(p)     ##结果为：point(x=1,y=2)

# 花色和数字
# Card = namedtuple('card',['suits','number'])
# c1 = Card('红桃',2)
# print(c1)
# print(c1.number)
# print(c1.suits)


# 队列
# import queue
# q = queue.Queue()   ##创建队列
# q.put(10)           ##往队列中放入值
# q.put(6)
# q.put(5)
# print(q)            ##结果为：<queue.Queue object at 0x00000000011FDB70>
# print(q.get())      ##取值，根据先进先出原则，取出来的是10；待队列中值全部取完后，会阻塞等待
# print(q.get())
# print(q.get())
# print(q.qsize())   ##当前队列大小，每取出一个值，队列大小减1

# 案例2：deque（双端队列）
# 使用list存储数据时，按索引访问元素很快，但是插入和删除元素就很慢了，
# 因为list是线性存储，数据量大的时候，插入和删除效率很低。
# deque是为了高效实现插入和删除操作的双向列表，适合用于队列和栈：
# from collections import deque
# dq = deque([1,2])
# dq.append('a')   #从后面放数据
# dq.appendleft('b')  #从前面放数据
# dq.insert(1,3)   ##插队,在索引1的位置插入3
# dq.pop()    ##从后面取数据
# dq.popleft()  ##从前面取数据
# print(dq)


# 案例3：OrderedDict()  有序字典
# 使用dict时，Key是无序的。在对dict做迭代时，我们无法确定Key的顺序。
# 如果要保持Key的顺序，可以用OrderedDict：
# from collections import OrderedDict
# od = OrderedDict([('a',1),('b',2),('c',3)])
# print(od)       ##OrderedDict的key是有序的
# print(od['a'])
# for k in od:
#     print(k)


#案例4 defaultdict
# from collections import defaultdict
# # values = [11, 22, 33,44,55,66,77,88,99,90]
# # my_dict = defaultdict(list)
# # for value in  values:
# #     if value>66:
# #         my_dict['k1'].append(value)
# #     else:
# #         my_dict['k2'].append(value)


#案例5：Counter 计数器
# Counter类的目的是用来跟踪值出现的次数。它是一个无序的容器类型，以字典的键值对形式存储，
# 其中元素作为key，其计数作为value。计数值可以是任意的Interger（包括0和负数）。
# Counter类和其他语言的bags或multisets很相似。
from collections import Counter
c = Counter('abcdeabcdabcaba')
print(c)           ##结果为：Counter({'a': 5, 'b': 4, 'c': 3, 'd': 2, 'e': 1})

