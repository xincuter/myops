#超过最大递归限制的报错
#只要写递归函数，必须要有结束条件。

#返回值
'''
不要只看到return就认为已经返回了。要看返回操作时在递归到第几层的时候发生的，
然后返回给了谁。
如果不是返回给最外层函数，调用者就接收不到。
需要在分析，看如何把结果返回回来。
'''

#循环
#递归
#几乎所有的递归，都可以使用循环代替，只不过思考的逻辑会复杂些。


#斐波那契 #问第n个斐波那契数是多少？
'''
1,1,2,3,5,8   
#fib(6) = fib(5) + fib(4)
#fib(5) = fib(4) + fib(3)
#fib(4) = fib(3) + fib(2)
#fib(3) = fib(2) + fib(1)
#fib(2) = 1
#fib(1) = 1
'''
#使用递归实现：(双递归)
# def fib(n):
#     print(n)
#     if n == 1 or n == 2:
#         return 1
#     return fib(n-1) + fib(n-2)
#
# print(fib(10))

#使用单递归实现：
# def fib2(n,l = [0]):
#     l[0] += 1
#     if n == 1 or n == 2:
#         l[0] -= 1
#         return 1,1
#     else:
#         a,b = fib2(n-1)
#         l[0] -= 1
#         if l[0] == 0:
#             return a+b
#         return b,a+b
#
# print(fib2(6))

# 阶乘
    # 3!  3 * 2 * 1
    # 2!  2 * 1
    # 1!  1

def func(n):
    if n == 1:
        return 1
    return n * func(n-1)

print(func(10))