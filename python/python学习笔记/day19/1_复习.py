# 正则表达式
# 字符组 [字符]
# 元字符
    # \w \d \s
    # \W \D \S
    # . 除换行符外任意字符
    # \n \t
    # \b
    # ^ $ 匹配字符串的开始和结束
    # () 分组 是对多个字符组整体量词约束时候用的
        # 分组是有优先级的
        # findall
        # split
    # | 从左到右匹配，只要匹配上就不继续匹配了，所以要把长的放前面
    # [^] 除了字符组内的其他都匹配
# 量词
    # *      0~
    # +      1~
    # ?      0~1
    # {n}    n
    # {n,}   n~
    # {n,m}  n~m

# 转义的问题
# import re
# re.findall(r'\\s',r'\s')

# 惰性匹配  （在量词后面加问号即可）
# 如：.*?abc   一直取遇到abc就停


# re模块
# import re
# re.findall('\d','awir17948jsdc',re.S)
# 返回值：列表，列表中是所有匹配到的项

# ret = re.search('\d(\w)+','awir17948jsdc')    //结果为：17948jsdc
# ret = re.search('\d(?P<name>\w)+','awir17948jsdc')    //结果为：17948jsdc    【?P<name>表示给分组命名】
# 找整个字符串，遇到匹配的就返回，遇不到就None
# 如果有返回值ret.group()就可以取到值
# 取分组中的内容：ret.group(1)  /  ret.group('name')


# match
# 从头开始匹配，匹配上了就返回，匹配不上就是None，如果匹配上了，使用.group取值；

# 分割 split
# 替换 sub 和 subn
# finditer  返回迭代器
# compile 编译：正则表达式很长且要多次使用时，进行编译会很方便


# 案例1
# import re
# ret = re.search("<(?P<tag_name>\w+)>\w+</(?P=tag_name)>","<h1>hello</h1>")
# # 还可以在分组中利用?<name>的形式给分组起名字；
# # 获取的匹配结果可以直接用group('名字')拿到对应的值
# print(ret.group('tag_name'))  #结果为：h1
# print(ret.group())      #结果为：<h1>hello</h1>
#
# ret1 = re.search(r"<(\w+)>\w+</\1>","<h1>hello</h1>")
# # 如果不给组起名字，也可以用\序号来找到对应的组，表示要找的内容和前面的组内容一致
# # 获取的匹配结果可以直接用group(序号)拿到对应的值。
# print(ret1.group(1))
# print(ret1.group())


# 案例2
import re

ret=re.findall(r"\d+","1-2*(60+(-40.35/5)-(-4*3))")
print(ret) #['1', '2', '60', '40', '35', '5', '4', '3']
ret=re.findall(r"-?\d+\.\d*|(-?\d+)","1-2*(60+(-40.35/5)-(-4*3))")
print(ret) #['1', '-2', '60', '', '5', '-4', '3']
ret.remove("")
print(ret) #['1', '-2', '60', '5', '-4', '3']
