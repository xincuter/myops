# 时间模块：time
# import time
# 常用方法
# 1.time.sleep(secs)   ## (线程)推迟指定的时间运行。单位为秒。
# 2.time.time()  ##获取当前时间戳

# python中时间的三种格式：
# 字符串 --- 格式化时间：给人看的
# 时间戳时间 --- float时间：计算机看的
# 结构化时间 --- 院子组：计算用的

import time
print(time.strftime("%Y-%m-%d %H:%M:%S")) #year month day Hour Minute Second    结构化时间转换成字符串时间

# 时间戳和结构化时间
t = time.time()
print(t)
print(time.localtime(1600000000))        ##结构化时间
print(time.mktime(time.localtime()))   ##把结构化时间转成时间戳

print(time.strptime("2017-03-16","%Y-%m-%d"))     ##把字符串时间转换成结构化时间



# 随机数模块：random
import random
#随机小数
random.random()      # 大于0且小于1之间的小数  结果为：0.7664338663654585
random.uniform(1,3) #大于1小于3的小数  结果为：1.6270147180533838

#随机整数
random.randint(1,5)  # 大于等于1且小于等于5之间的整数
random.randrange(1,10,2) # 大于等于1且小于10之间的奇数


#随机选择一个返回
random.choice([1,'23',[4,5]])  # #1或者23或者[4,5]
#随机选择多个返回，返回的个数为函数的第二个参数
random.sample([1,'23',[4,5]],2) # #列表元素任意2个组合 结果为：[[4, 5], '23']


#打乱列表顺序
item=[1,3,5,7,9]
random.shuffle(item) # 打乱次序
item #[5, 1, 3, 7, 9]
random.shuffle(item)
item   ##[5, 9, 7, 1, 3]