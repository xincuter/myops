'''学习内容'''
#re模块
# 正则表达式  ---  字符串匹配
#学习正则表达式
#学习使用re模块来操作正则表达式

##正则学习在线工具：https://tools.chinaz.com/regex
##正则学习博客文章：https://www.cnblogs.com/Eva-J/articles/7228075.html#_label10
##书籍：正则指引

'''匹配手机号案例'''
###不使用正则表达式，代码如下：
# def check_phonenum():
#     while True:
#         phone_number = input("please input your phone number: ")
#         if len(phone_number) == 11 \
#                 and phone_number.isdigit() \
#                 and (phone_number.startswith('13') \
#                 or phone_number.startswith('14') \
#                 or phone_number.startswith('15') \
#                 or phone_number.startswith('18')):
#             print("Congradulations, it's valid phonenumber.")
#             exit(0)
#         else:
#             print("Sorry, it's invalid phonenumber.")
#
# check_phonenum()


###使用正则表达式匹配，代码如下：
# import re
# def check_phonenum2():
#     while True:
#         phone_number = input("please input your phone number: ")
#         if re.match('^(13|14|15|18)[0-9]{9}$',phone_number):
#             print("Congradulations, it's valid phonenumber.")
#             exit(0)
#         else:
#             print("Sorry, it's invalid phonenumber.")
#
# check_phonenum2()


##re模块，主要方法如下：
# findall: 返回所有满足匹配条件的结果，放在列表中
# match: 从头开始匹配，如果正则规则从头开始可以匹配上，就返回一个变量。匹配的内容需要使用group()方法才能显示；如果没有
#找到，那么返回None，调用group会报错。(仅在字符串开始处进行匹配)
'''
search：从前往后找，找到一个就返回，返回的变量需要调用group()才能拿到结果,
如果没有找到，那么返回None，调用group()方法会报错。
'''


##例子1
import re
# ret = re.findall('a','eva egon yuan') #返回所有满足匹配条件的结果，放在列表中
# print(ret)   #结果为：['a','a']

##例子2：
# ret = re.search('a','eva egon yuan')
# ret1 = re.search('j','eva egon yuan')
# print(ret.group())    ##结果为：a
# print(ret1.group())   ##结果为：直接报错了

##例子3：
# ret = re.match('a','eva egon yuan')   ##结果为：None
# ret = re.match('ev','eva egon yuan')  ##结果为：ev
# ret = re.match('[a-z]+','eva egon yuan')  ##结果为：eva
# if ret:
#     print(ret.group())


##例子4：
# ret = re.split('[ab]', 'abcd')  # 先按'a'分割得到''和'bcd',在对''和'bcd'分别按'b'分割
# print(ret)  # 结果为：['', '', 'cd']
#
# ret = re.sub('\d', 'H', 'eva3egon4yuan4', 1)#将数字替换成'H'，参数1表示只替换1个
# print(ret) #结果为：evaHegon4yuan4
#
# ret = re.subn('\d', 'H', 'eva3egon4yuan4')#将数字替换成'H'，返回元组(替换的结果,替换了多少次)
# print(ret)   #结果为：('evaHegonHyuanH', 3)

# obj = re.compile('\d{3}')  #将正则表达式编译成为一个 正则表达式对象，规则要匹配的是3个数字
# ret = obj.search('abc123eeee') #正则表达式对象调用search，参数为待匹配的字符串
# print(ret.group())  #结果 ： 123
#
# import re
# ret = re.finditer('\d', 'ds3sy4784a')   #finditer返回一个存放匹配结果的迭代器
# print(ret)  # <callable_iterator object at 0x10195f940>
# print(next(ret).group())  #查看第一个结果
# print(next(ret).group())  #查看第二个结果
# print([i.group() for i in ret])  #查看剩余的左右结果


##注意：案例5 -- 取指定的分组，相当于sed中的后向引用（\1 , \2）
# import re
# # ret = re.search('^[1-9](\d{14})(\d{2}[0-9x])?$','110105199912122277')
# # print(ret.group())   ##结果为：110105199912122277'
# # print(ret.group(1))  ##结果为：10105199912122 【取出的是第一个分组的内容，相当于sed中的后向引用】
# # print(ret.group(2))  ##结果为：277


##注意：案例6 -- findall分组优先级的问题
# import re
# ret = re.findall('www.(baidu|oldboy).com','www.oldboy.com')
# print(ret)    ##结果为：['oldboy']
# # （执行结果并不是我们期望的['www.oldboy.com']，原因是：findall会优先把分组中的内容返回,如果想要匹配结果,取消权限即可，且findall没有像search那样的group()方法）
#
# ret1 = re.findall('www.(?:baidu|oldboy).com', 'www.oldboy.com')
# print(ret1)  # 结果为：['www.oldboy.com'] ，分组里的?:取消分组优先


##注意：案例7：split分组优先的问题
ret=re.split("\d+","eva3egon4yuan")
print(ret) #结果 ： ['eva', 'egon', 'yuan']

ret1=re.split("(\d+)","eva3egon4yuan")
print(ret1) #结果 ： ['eva', '3', 'egon', '4', 'yuan']

#在匹配部分加上（）之后所切出的结果是不同的，
#没有（）的没有保留所匹配的项，但是有（）的却能够保留了匹配的项，
#这个在某些需要保留匹配部分的使用过程是非常重要的。
